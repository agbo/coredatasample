//
//  ManagedStore.swift
//  CoreDataSample
//
//  Created by guille on 25/06/14.
//  Copyright (c) 2014 Guillermo Gonzalez. All rights reserved.
//

import CoreData

class ManagedStore {
    let persistentStoreCoordinator: NSPersistentStoreCoordinator!
    let managedObjectModel: NSManagedObjectModel!
    let managedObjectContext: NSManagedObjectContext!
    
    init(path: String) {
        managedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        var error: NSError?
        
        let store = persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType,
            configuration: nil,
            URL: NSURL(fileURLWithPath: path),
            options: [
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true
            ],
            error: &error)
        
        if store {
            managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
            managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        } else {
            // TODO: handle error
        }
    }
    
    func temporaryManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType) -> NSManagedObjectContext! {
        let context = NSManagedObjectContext(concurrencyType: concurrencyType)
        context.persistentStoreCoordinator = persistentStoreCoordinator
        
        return context
    }
}
