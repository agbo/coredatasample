//
//  Superhero.swift
//  CoreDataSample
//
//  Created by guille on 25/06/14.
//  Copyright (c) 2014 Guillermo Gonzalez. All rights reserved.
//

import CoreData

class Superhero : NSManagedObject {
    @NSManaged var name: String
    @NSManaged var secretIdentity: String
    
    class var entityName : String {
        return "Superhero"
    }
    
    class func fetchRequest() -> NSFetchRequest! {
        return NSFetchRequest(entityName: self.entityName)
    }
    
    class func insert(inManagedObjectContext context: NSManagedObjectContext) -> Superhero! {
        return NSEntityDescription.insertNewObjectForEntityForName(self.entityName, inManagedObjectContext: context) as Superhero
    }
}
